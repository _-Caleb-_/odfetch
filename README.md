# ODFetch
odfetch is a sysinfo terminal based script, like neofetch, ufetch or pfetch for OpenDingux Devices.

Tested (and works fine) with the RG350 (ROGUE CFW)

![](https://codeberg.org/_-Caleb-_/odfetch/raw/branch/main/screenshot015.png)

**OpenDingux** Ascii Logo

![](https://codeberg.org/_-Caleb-_/odfetch/raw/branch/main/screenshot017.png)

**Rogue** Ascii Logo

If you wish please, fork and/or pull something :-)

Kind regards!